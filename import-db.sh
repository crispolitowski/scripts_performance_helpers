#!/bin/bash
# Script para importar o banco remoto

#-----
# Config
#-----
SSH_USER=$1
SSH_HOST=$2
REMOTE_DBUSER=$3
REMOTE_DBPASS=$4
DBNAME=$5

#-----

LOCAL_FILEPATH="/tmp"
REMOTE_FILEPATH="/tmp"
DATE=$(date +"%F-%H%M")
SQL_FILENAME="$DBNAME-$DATE.sql"

echo
echo "  => DATABASE SYNC SCRIPT"
echo "  => this script will import the remote database $DBNAME from host $SSH_HOST"
echo "  => to the local database $DBNAME"
echo

echo -n "dumping remote database to temporary file $REMOTE_FILEPATH/$SQL_FILENAME.gz..."
ssh $SSH_USER@$SSH_HOST "mysqldump -u $REMOTE_DBUSER -p$REMOTE_DBPASS $DBNAME | gzip > $REMOTE_FILEPATH/$SQL_FILENAME.gz"
echo "done"

echo "importing remote dump file (saving it to a local temporary file)..."
scp $SSH_USER@$SSH_HOST:$REMOTE_FILEPATH/$SQL_FILENAME.gz $LOCAL_FILEPATH
echo "done!"

echo -n "deleting remote dump file $REMOTE_FILEPATH/$SQL_FILENAME.gz..."
ssh $SSH_USER@$SSH_HOST "rm -f $REMOTE_FILEPATH/$SQL_FILENAME.gz"
echo "done"

echo -n "uncompressing dump file $SQL_FILENAME.gz..."
cd $LOCAL_FILEPATH ; gunzip "$SQL_FILENAME.gz"
echo "done"

echo -n "importing dump file to the local MySQL server (database $DBNAME)..."
mysql $DBNAME < $SQL_FILENAME
echo "done"
