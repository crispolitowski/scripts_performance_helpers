#!/bin/bash

echo "Copiando as permissões dos usuários mysql"
# ssh root@sistema.bbcsaude.com 'mysql -u bbc -py7DzWXMxYbYHauSz -e "SHOW GRANTS FOR 'bbc'@'localhost';"'
permissao=$(ssh $SSH_USER@$SSH_HOST << EOF
    mysql -u ${REMOTE_DBUSER} -p${REMOTE_DBPASS} -e "SHOW GRANTS FOR ${REMOTE_DBUSER}@'
    localhost';"
EOF)
echo
echo ${permissao}
# 's/ /---/3' permissao | sed 's/^.*---//'
# res=$(sed 's/ /---/3' permissao | sed 's/^.*---//')
# echo ${res} > res
# mysql -e "${res}"