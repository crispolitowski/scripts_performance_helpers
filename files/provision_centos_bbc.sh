#!/bin/bash

yum -y update
yum -y upgrade

rpm -Uvh https://mirror.webtatic.com/yum/el6/latest.rpm

yum -y update

yum -y install git

yum -y install php55w php55w-opcache php55w-xml php55w-mbstring php55w-mysql
yum -y install httpd-devel
yum -y install mysql mysql-devel mysql-server

# copia os arquivos de configuracao
cd /vagrant/confs

cp -f php.ini      /etc/
cp -f my.cnf       /etc/
cp -f httpd.conf   /etc/
cp -f ssl.conf     /etc/
cp -f php.conf     /etc/
cp -f gd_bundle-g2-g1.crt          /etc/pki/tls/certs/
cp -f sistema.bbcsaude.com.crt     /etc/pki/tls/certs/
cp -f sistema.bbcsaude.com.key     /etc/pki/tls/private/

# copia a chave privada para acesso ssh
cd ..
mkdir -p ~/.ssh/
cp ssh/id_rsa ~/.ssh/
chmod 0600 ~/.ssh/id_rsa

# inicia os servicos
service mysqld start
service httpd start