#!/bin/bash
# Executado na máquina Host, usado para criar os diretórios
# * nome_do_cliente
    # - scripts (submodule)
    # - confs (copiado via SCP)
    # - source
    # - vagrantfile
    # - docs
    # - linux
    # - apache
    # - myaql
    # - php
    #     + benchs
    #     + reports

cp files/Vagrantfile_model ./../Vagrantfile

cd ..

mkdir -p docs


# mkdir -p linux/confs
mkdir -p linux/benchs
mkdir -p linux/reports

# mkdir -p webserver/confs
mkdir -p webserver/benchs
mkdir -p webserver/reports

# mkdir -p mysql/confs
mkdir -p mysql/benchs
mkdir -p mysql/reports

# mkdir -p php/confs
mkdir -p php/benchs
mkdir -p php/reports

mkdir -p source

mkdir -p ssh

mkdir -p ssl



