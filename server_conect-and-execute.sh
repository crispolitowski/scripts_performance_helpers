#!/bin/bash

# ssh $USER@$HOST 'bash -s' < server_copy-conf-files.sh "$@"

while getopts "u:h:f:" OPTION; do
  case $OPTION in
    u) USER="${OPTARG}"
       # exit 1
       ;;
    h) HOST="${OPTARG}"
       # exit 1
       ;;
    f) FILES="${OPTARG}"
       # exit 1
       ;;
    *) echo ">>> Unimplemented option $OPTION."
       ;;
  esac
done

echo $USER $HOST $FILES
