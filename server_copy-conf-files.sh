#!/bin/bash
# Procura pelos arquivos passados por parametro e coloca-os nos devidos diretórios

cd /tmp
mkdir -p confs

for var in "$@"
do
    echo "Procurando o arquivo: $var"
    # echo "Diretório de armazenamento: $output_dir"
    res=$(find / -name $var)
    for file in $res
    do
        echo "  Copiado: $file"
        cp --preserve=all --backup=t $file confs

    done

done

echo  "Copiando ĺista de extensões do PHP"
php -m > /tmp/confs/php_extensions_enabled

echo  "Copiando lista de módulos do Apache"
apachectl -M > /tmp/confs/apache_modules_enabled

# echo "Copiando as permissões dos usuários mysql"
# mysql -e  "SHOW GRANTS FOR 'root'@'localhost';" > permissoes
